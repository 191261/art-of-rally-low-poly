//#ifndef TEXTURE_HPP
//#define TEXTURE_HPP
//
//#include<glad/glad.h>
//#include<GLFW/glfw3.h>
//
//#include<assimp/scene.h>
//
//#include<stb_image.h>
//
//class Texture{
//    public:
//        Texture();
//        Texture(unsigned int id, std::string path, std::string type);
//
//        void generate();
//        void load(bool flip = true);
//
//        void bind();
//
//        unsigned int id;
//        std::string type;
//        std::string path;
//};
//
//#endif